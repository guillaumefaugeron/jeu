﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnitySampleAssets._2D;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update

    public float speed ;
    public int damage ;
    public Rigidbody2D rb;
    public AudioClip playerDamageSound;

    void Start()
    {

        rb.velocity = transform.right * speed;
    }


    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        PlayerDamage player2 = hitInfo.GetComponent<PlayerDamage>();
        PlayerDamage player1 = hitInfo.GetComponent<PlayerDamage>();

        if (hitInfo.GetComponentInParent<Collider2D>().name == "Player 1")
        {
            player1.TakeDamage(damage);
            MakeSound(playerDamageSound);

        }
        else if (hitInfo.GetComponentInParent<Collider2D>().name == "Player 2")
        {
            player2.TakeDamage(damage);
            MakeSound(playerDamageSound);

        }

        

        
        Destroy(gameObject);
    }
    private void MakeSound(AudioClip originalClip)
    {
        AudioSource.PlayClipAtPoint(originalClip, transform.position, 0.1f);
    }
}
