﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.PlayerLoop;
using Random = System.Random;

public class PlayerMovement : MonoBehaviour
{
	public CharacterController2D controller;
	public Animator animator;

	public float runSpeed = 40f;

	float horizontalMove = 0f;
	bool jump = false;
	bool crouch = false;

	public string jumpKey;
	public string HorizontalMoveInput;


	// Update is called once per frame
	void Update () {

		horizontalMove = Input.GetAxisRaw(HorizontalMoveInput) * runSpeed;

		animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

		if (Input.GetButtonDown(jumpKey))
		{
			jump = true;
			animator.SetBool("Jump", true);
		}
		

	}
	
	
	public void OnLanding ()
	{
		animator.SetBool("Jump", false);
	}
	

	void FixedUpdate ()
	{
		// Move our character
		controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
		jump = false;
	}
}
