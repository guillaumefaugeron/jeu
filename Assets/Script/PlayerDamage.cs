﻿using System.Data;
using Mono.Data.Sqlite;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnitySampleAssets._2D
{
    public class PlayerDamage : MonoBehaviour
    {
        public int maxHealth = 200;
        public int currentHealth;
        public HealthBar healthBar;
        private bool showPopUp = false;

    
        private void Start()
        {
            currentHealth = maxHealth;
            healthBar.SetMaxHealth(maxHealth);
        
            Debug.Log("HP set at "+currentHealth);
        
        }

        

        public void TakeDamage (int damage)
        {
            Debug.Log(gameObject+"  aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+currentHealth);
            
            currentHealth =  currentHealth-damage;
            healthBar.SetHealth(currentHealth);
            
            Debug.Log(gameObject+"  aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+currentHealth);

            if (currentHealth <= 0)
            {
                showPopUp = true;
                Die();
                // Time.timeScale = 0f;
                // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
 
                
            }
        }
        
        
        void Die ()
        {
            Debug.Log(gameObject+" is dead");
            gameObject.SetActive(false);
        }
        
        
        
        
        
        
        
    }
}