﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine;


public class Game : MonoBehaviour
{
    private int endGame =0;
    public GameObject player1;
    public GameObject player2;
    public AudioClip sample1;
    public AudioClip sample2;
    public AudioClip sample3;
    private int sampleNumber;

    void Start()
    {
        sampleNumber = UnityEngine.Random.Range(1, 4);
        if (sampleNumber == 1)
        {
            MakeSound(sample1);
            Debug.Log("play sample :"+ sampleNumber);
        }
        if (sampleNumber == 2)
        {
            MakeSound(sample2);
            Debug.Log("play sample :"+ sampleNumber);

        }
        if (sampleNumber == 3)
        {
            MakeSound(sample3);
            Debug.Log("play sample :"+ sampleNumber);

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (endGame == 0)
        {
            if (player1.activeSelf == false)
            {
                InsertPlayerStat("Player 2");
                endGame =+ 1;

            }
            if (player2.activeSelf == false)
            {
                InsertPlayerStat("Player 1");
                endGame =+ 1;
            }
        }
    }
    
    void InsertPlayerStat(string player)
    {
        // Create database
        string connection = "URI=file:" + Application.persistentDataPath + "/scoreBoard2" ;
        Debug.Log("path de la bdd sqlite " + Application.persistentDataPath);
		
        // Open connection
        IDbConnection dbcon = new SqliteConnection(connection);
        dbcon.Open();
        
        // Create table
        IDbCommand dbcmd;
        dbcmd = dbcon.CreateCommand();
        string q_createTable = "CREATE TABLE IF NOT EXISTS playerStats (id INTEGER PRIMARY KEY, player TEXT)";
		
        dbcmd.CommandText = q_createTable;
        dbcmd.ExecuteReader();

        // Insert values in table
        IDbCommand insert = dbcon.CreateCommand();
        insert.CommandText = "INSERT INTO playerStats (player) VALUES (@param1)";
        insert.Parameters.Add(new SqliteParameter("@param1", player));
        insert.ExecuteNonQuery();
        dbcon.Close();

    }
    private void MakeSound(AudioClip originalClip)
    {
        AudioSource.PlayClipAtPoint(originalClip, transform.position,100);
    }
    
    
}
