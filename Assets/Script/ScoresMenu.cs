﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.UI;

public class ScoresMenu : MonoBehaviour
{
    public Text Scoreplayer1;
    public Text Scoreplayer2;
    private string VP1;
    private string VP2;
    // Start is called before the first frame update
    void Start()
    {
        GetScore();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void GetScore()
    {
        string connection = "URI=file:" + Application.persistentDataPath + "/scoreBoard2" ;
        IDbConnection dbcon = new SqliteConnection(connection);
        dbcon.Open();
        IDbCommand dbcmd;
        dbcmd = dbcon.CreateCommand();
        string q_createTable = "CREATE TABLE IF NOT EXISTS playerStats (id INTEGER PRIMARY KEY, player TEXT)";
        dbcmd.CommandText = q_createTable;
        dbcmd.ExecuteReader();
        // compte le nombre de player 1 et plauyer 2 afin de determiner le nombres de victoires
        IDataReader reader;
        IDbCommand countP1 = dbcon.CreateCommand();
        countP1.CommandText = "SELECT count(*) FROM playerStats WHERE player='Player 1'";
        reader = countP1.ExecuteReader();
        VP1 = reader[0].ToString();
        IDbCommand countP2 = dbcon.CreateCommand();
        countP2.CommandText = "SELECT count(*) FROM playerStats WHERE player='Player 2'";
        reader = countP2.ExecuteReader();
        VP2 = reader[0].ToString();
        Scoreplayer1.text = VP1;
        Scoreplayer2.text = VP2;

        dbcon.Close();
    }
    public void Reset()
    {
        string connection = "URI=file:" + Application.persistentDataPath + "/scoreBoard2" ;
        IDbConnection dbcon = new SqliteConnection(connection);
        dbcon.Open();
        IDbCommand dbcmd;
        dbcmd = dbcon.CreateCommand();
        string q_createTable = "DROP TABLE IF EXISTS playerStats";
        dbcmd.CommandText = q_createTable;
        dbcmd.ExecuteReader();
        GetScore();
    }
}
