using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public AudioClip sample1;
    public AudioClip sample2;
    private int sampleNumber;
    public float volume;
    // Start is called before the first frame update
    void Start()
    {
        sampleNumber = UnityEngine.Random.Range(1, 2);
        if (sampleNumber == 1)
        {
            MakeSound(sample1);
            Debug.Log("play sample :"+ sampleNumber);

        }
        if (sampleNumber == 2)
        {
            MakeSound(sample2);
            Debug.Log("play sample :"+ sampleNumber);

        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void MakeSound(AudioClip originalClip)
    {
        volume = 0.2f;
        AudioSource.PlayClipAtPoint(originalClip, transform.position, volume );
    }
}
