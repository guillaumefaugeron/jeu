﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine;
using UnitySampleAssets._2D;

public class DeathPopup : MonoBehaviour
{
    // Start is called before the first frame update

    public PlayerDamage player2;
    public PlayerDamage player1;
    public GameObject DeathPopupUi;
    public GameObject Player2Ui;
    public GameObject Player1Ui;
    void Start()
    {
        // Crée la base de donnée 

        

    }

    // Update is called once per frame
    void Update()
    {
        if (player1.currentHealth <= 0)
        {


            DeathPopupUi.SetActive(true);
            // Time.timeScale = 0f;
            Player2Ui.SetActive(true);
            
        
        } 
        if (player2.currentHealth <= 0)
        {
            DeathPopupUi.SetActive(true);
            // Time.timeScale = 0f;
            Player1Ui.SetActive(true);


        }
    }


}
