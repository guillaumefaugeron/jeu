﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathMenu : MonoBehaviour
{
    
    public GameObject player1;
    public GameObject player2;

    public void MainMenu()
    {
        player1.SetActive(true);
        player2.SetActive(true);
        SceneManager.LoadScene(0);
    }
    
    public void RePlay()
    {
        player1.SetActive(true);
        player2.SetActive(true);
        SceneManager.LoadScene(1);
    }
    public void QuitGame()
    {
        Debug.Log("quit");
        Application.Quit();
    }
}
